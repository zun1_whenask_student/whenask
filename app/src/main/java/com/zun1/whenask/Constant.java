package com.zun1.whenask;

import android.net.wifi.WifiManager;

import retrofit2.http.PUT;

/**
 * Created by dell on 2016/6/30.
 */
public class Constant {
    public final static String USERNAME = "username";
    public final static String TRUSTNAME = "trustname";
    public final static String FIRSTNAME = "firstname";
    public final static String LASTNAME = "lastname";
    public final static String LANGUAGE_CODE = "language_code";
    public final static String IDENTIFICATION = "identification";
    public final static String PASSWORD = "password";
    public final static String EMAIL  = "email";
    public final static String PHONE = "phone";
    public final static String ADDRESS = "address";

    public final static String TEACHERINTT = "teacherIntroduce";
    public final static String REGISTERURL = "http://120.76.28.144:7000/snippets/signup/";
    public final static String LOGINURL = "http://120.76.28.144:7000/mobileapi/public/signin/";
    public final static  String AUTOLOGINURL = "http://120.76.28.144:7000/mobileapi/public/autosignin/";
}
