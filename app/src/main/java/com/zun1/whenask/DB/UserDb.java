package com.zun1.whenask.DB;

import android.content.Context;
import android.content.SharedPreferences;

import com.zun1.whenask.Constant;

import retrofit2.http.PUT;

/**
 * Created by dell on 2016/7/4.
 */
public class UserDb {

    private final String fileName = "user.ini";
    private Context context;
    SharedPreferences sharedPreferences;

    private static UserDb userDb = null;
    public static UserDb instance(){
        if(userDb == null){
            synchronized (UserDb.class){
                userDb = new UserDb();
            }
        }
        return userDb;
    }
    private UserDb(){

    }
    public void init(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(fileName,context.MODE_PRIVATE);
    }
    public String getLoginImmportPassword(){
        return sharedPreferences.getString(Constant.PASSWORD,"");
    }
    public String getLoginImportName(){
         return sharedPreferences.getString(Constant.USERNAME,"");
    }
    public void setUserInfo(String username,String firstName,String lastName,String password,String address,
                            String id){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.USERNAME,username);
        editor.putString(Constant.FIRSTNAME,firstName);
        editor.putString(Constant.LASTNAME,lastName);
        editor.putString(Constant.PASSWORD,password);
        editor.putString(Constant.ADDRESS,address);
        editor.putString(Constant.IDENTIFICATION,id);
        editor.commit();
    }

}
