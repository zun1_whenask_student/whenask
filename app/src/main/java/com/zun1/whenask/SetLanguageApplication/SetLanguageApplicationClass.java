package com.zun1.whenask.SetLanguageApplication;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import java.lang.annotation.Target;
import java.util.Locale;

/**
 * Created by zun1user7 on 2016/7/4.
 */

public class SetLanguageApplicationClass extends Application {

    private String mac;
    private String imem;
    private float lot;
    private float lat;
    private LocationManager locationManager;
    Location location = null;
    @Override
    public void onCreate() {
        super.onCreate();
        setLanguage(Locale.TAIWAN);

    }
   public  void setLanguage(Locale language){
       String languageToLoad  = "zh";
       Locale locale = new Locale(languageToLoad);
       Locale.setDefault(locale);
       Configuration config = getResources().getConfiguration();
       DisplayMetrics metrics = getResources().getDisplayMetrics();
       config.locale = language;
       getResources().updateConfiguration(config, metrics);
    }

    public String getMac(){
        WifiManager manager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiInfo infor = manager.getConnectionInfo();
        mac = infor.getMacAddress();
        return mac;
    }

    public  String getImem(){
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imem = telephonyManager.getDeviceId();
        return imem;
    }
    public float getLat(){
       locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if( PackageManager.PERMISSION_GRANTED  ==ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION))
              location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
        return (float) location.getLatitude();
    }

    public float getLot(){
        if( PackageManager.PERMISSION_GRANTED  ==ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION))
            location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
        return (float) location.getLongitude();
    }
}
