package com.zun1.whenask.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import com.zun1.whenask.DB.entity.TeacherDb;
import com.zun1.whenask.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2016/7/4.
 */
public class Introduce_teacher extends AppCompatActivity {

    @BindView(R.id.intr_edit)
    EditText editText;

    @BindView(R.id.intr_submit)
    Button button;

    TeacherDb teacherDb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introduce_teacher);
        ButterKnife.bind(this);
        teacherDb =  TeacherDb.instance();
        teacherDb.init(this);

    }
    @OnClick(R.id.intr_submit)
    public void submitTeacher(){
        String teacherInfo = editText.getText().toString();
        teacherDb.setTeacherInfo(teacherInfo);
    }
}
