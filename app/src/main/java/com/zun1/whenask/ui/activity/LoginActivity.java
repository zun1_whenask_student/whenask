package com.zun1.whenask.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import com.zun1.whenask.R;
import com.zun1.whenask.service.RegisterService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.register_button)
    Button register;

    @BindView(R.id.user)
    Button user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ask_user);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.register_button)
    public void register(){
        Intent register = new Intent(this,RegisterActivity.class);
        startActivity(register);
    }

    @OnClick(R.id.user)
    public void user(){
        Intent intent = new Intent(this,LoginImportActivity.class);
        startActivity(intent);
    }
}
