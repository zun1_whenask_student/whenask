package com.zun1.whenask.ui.activity;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.zun1.whenask.DB.UserDb;
import com.zun1.whenask.R;
import com.zun1.whenask.SetLanguageApplication.SetLanguageApplicationClass;
import com.zun1.whenask.ToolsClass.HttpRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class LoginImportActivity extends AppCompatActivity implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{
    private Button forgetpasswordBtn;
    private EditText username,password;
    private Button submitBtn,resetBtn;
    private UserDb userDb;
    private CheckBox saveBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        userDb = UserDb.instance();
        userDb.init(this);
        findViewById();
        setOnClickListener();

    }
    //UI
    void findViewById(){
        forgetpasswordBtn = (Button)this.findViewById(R.id.forget_password);
        username = (EditText)this.findViewById(R.id.edText_login_username);
        password = (EditText)this.findViewById(R.id.edText_login_password);
        saveBtn = (CheckBox) this.findViewById(R.id.login_savebtn);
        resetBtn = (Button)this.findViewById(R.id.login_reset);
        submitBtn = (Button)this.findViewById(R.id.login_submit);
    }
    //监听器
    void setOnClickListener(){
        forgetpasswordBtn.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        resetBtn.setOnClickListener(this);
        saveBtn.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b){
            username.setText(userDb.getLoginImportName());
            password.setText(userDb.getLoginImmportPassword());
        }else {
            username.setText("");
            password.setText("");
        }
    }

    //按钮监听器
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.forget_password:
                Intent intent = new Intent(LoginImportActivity.this,ForgetPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.login_submit:
                submitAction();
                break;
            case R.id.login_reset:
                username.setText("");
                password.setText("");
                break;
        }
    }
    //提交按钮
    void submitAction(){
        Log.i("---->","提交");
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Map<String,String>> list = new ArrayList<Map<String, String>>();
                Map<String,String> mapusername = new HashMap<String, String>();
                mapusername.put("username",username.getText().toString().trim());
                list.add(mapusername);
                Map<String,String> mappassword = new HashMap<String, String>();
                mappassword.put("password",password.getText().toString().trim());
                list.add(mappassword);
                Map<String,String> maplon = new HashMap<String, String>();
                maplon.put("lon","112.1111");
                list.add(maplon);
                Map<String,String> maplat = new HashMap<String, String>();
                maplat.put("lat","35.2222");
                list.add(maplat);
                Map<String,String> mapaddrmac = new HashMap<String, String>();
                SetLanguageApplicationClass applicationClass = (SetLanguageApplicationClass) getApplication();
                mapaddrmac.put("addrmac",applicationClass.getMac());
                list.add(mapaddrmac);
                Map<String,String> mapdevicenum = new HashMap<String, String>();
                mapdevicenum.put("devicenum",applicationClass.getImem());
                list.add(mapdevicenum);
                Map<String,String> mapplatform = new HashMap<String, String>();
                mapplatform.put("platform","1");
                list.add(mapplatform);
                String url = "http://120.76.28.144:7000/mobileapi/public/signin/";
                HttpRequest.postRequestBody(list,url);
            }
        }).start();
    }
}
