package com.zun1.whenask.ui.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.zun1.whenask.Constant;
import com.zun1.whenask.DB.UserDb;
import com.zun1.whenask.R;
import com.zun1.whenask.SetLanguageApplication.SetLanguageApplicationClass;
import com.zun1.whenask.ToolsClass.HttpRequest;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{
    private EditText userName,firstname,lastname,password,email,iphonenumber,address,id;
    private CheckBox checkBox;
    private Button submit,reset;
    private Boolean isSureIphoneNumber,isSureEmail,isSurePassWord,isSureUsername,isSureId;
    private static final String TAG = "Register";
    private String userNames,firstnames,lastnames,passwords,emails,iphonenumbers,addresss,language;
    private Map<String,String> userItem;
    private List<Map<String,String>> userInfo;
    private UserDb userDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        userDb = UserDb.instance();
        userDb.init(this);
        findViewById();
        setOnClickListener();
    }
    void findViewById(){
        userName = (EditText)this.findViewById(R.id.edText_register_username);
        firstname = (EditText)this.findViewById(R.id.edText_register_firstname);
        lastname = (EditText)this.findViewById(R.id.edText_register_lastname);
        password = (EditText)this.findViewById(R.id.edText_register_password);
        email = (EditText)this.findViewById(R.id.edText_register_email);
        iphonenumber = (EditText)this.findViewById(R.id.edText_register_iphonenumber);
        address = (EditText)this.findViewById(R.id.edText_register_address);
        id = (EditText)this.findViewById(R.id.edText_register_identi);
        checkBox = (CheckBox)this.findViewById(R.id.is_savePassword);


        submit = (Button)this.findViewById(R.id.register_submit);
        reset = (Button)this.findViewById(R.id.register_reset);
    }
    void setOnClickListener(){
        submit.setOnClickListener(this);
        reset.setOnClickListener(this);
        checkBox.setOnCheckedChangeListener(this);
        id.addTextChangedListener(new IdentificationIs());
        iphonenumber.addTextChangedListener(new IphoneNumber());
        userName.addTextChangedListener(new NameChange());
        password.addTextChangedListener(new PasswordChange());
        email.addTextChangedListener(new EmailChange());

    }
    private String getEdit(EditText editText){
        return editText.getText().toString();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.register_submit:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<Map<String,String>> list = new ArrayList<Map<String, String>>();
                        Map<String,String> mapname = new HashMap<String, String>();
                        mapname.put(Constant.USERNAME,getEdit(userName));
                        list.add(mapname);
                        Map<String,String> maplastname = new HashMap<String, String>();
                        maplastname.put(Constant.LASTNAME,getEdit(lastname));
                        list.add(maplastname);
                        Map<String,String> mapfirstname = new HashMap<String, String>();
                        mapfirstname.put(Constant.FIRSTNAME,getEdit(firstname));
                        Map<String,String> mappassword = new HashMap<String, String>();
                        mappassword.put(Constant.FIRSTNAME,getEdit(firstname));
                        list.add(mappassword);
                        Map<String,String> mapemail = new HashMap<String, String>();
                        mapemail.put(Constant.EMAIL,getEdit(email));
                        list.add(mapemail);
                        Map<String,String> mapphone = new HashMap<String, String>();
                        mapphone.put(Constant.PHONE,getEdit(iphonenumber));
                        list.add(mapphone);
                        Map<String,String> mapaddress = new HashMap<String, String>();
                        mapaddress.put(Constant.ADDRESS,getEdit(address));
                        list.add(mapaddress);
                        Map<String,String> maplanguage_code = new HashMap<String, String>();
                        maplanguage_code.put("language_code","zh-cn");
                        list.add(maplanguage_code);
                        Map<String,String> maplon = new HashMap<String, String>();
                        maplon.put("lon","113");
                        list.add(maplon);
                        Map<String,String> maplat = new HashMap<String, String>();
                        maplat.put("lat","22");
                        list.add(maplat);
                        Log.i("list.size", String.valueOf(list.size()));
                        String url = Constant.LOGINURL;
                        HttpRequest.postRequestBody(list,url);
                    }
                }).start();
                break;
            case R.id.register_reset:
                break;
        }
    }

    class IdentificationIs implements TextWatcher{
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            isSureId = Pattern.matches("^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$"
            ,editable.toString());
            if (isSureId){
                Log.i(TAG,"ID-->" +isSureId);
            }else {
                Log.i(TAG,"id--->" + isSureId);
            }
        }
    }
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b){
            Log.i(TAG,"SAVEPASSWORD：：：："+ true);

            userDb.setUserInfo(getEdit(userName),getEdit(firstname),getEdit(lastname),"qweewqwe",getEdit(address),getEdit(id));
        }else {
            Log.i(TAG,"SAVEPASS::::"+false);
        }
    }

    class IphoneNumber implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            isSureIphoneNumber = Pattern.matches("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$",editable.toString());
            //isSureIphoneNumber = editable.toString().matches("^((13[0-9])|(15[^4,\\\\D])|(18[0,5-9]))\\\\d{8}$");

            if(isSureIphoneNumber){
                Log.i(TAG, "isSureIphoneNumber" + editable.toString()+"》"+isSureIphoneNumber.toString());
            }else {
                Log.i(TAG, "isSureIphoneNumber" +editable.toString()+"》"+ isSureIphoneNumber.toString());
            }
        }
    }

    class EmailChange implements TextWatcher{
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            isSureEmail = android.util.Patterns.EMAIL_ADDRESS.matcher(editable.toString()).matches();
            if(isSureEmail){
                Log.i(TAG, "isEmail-- " + editable.toString()+":::"+isSureEmail);
            }else {
                Log.i(TAG, "isEmail--" + editable.toString()+":::"+isSureEmail);
            }
        }
    }
    class PasswordChange implements TextWatcher{
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            isSurePassWord = Pattern.matches("^[a-zA-Z0-9]\\w{7,19}",editable.toString());
            if(isSurePassWord){
                Log.i(TAG, "password:::: " + editable.toString()+"::::"+isSurePassWord.toString());
            }else {
                Log.i(TAG, "password:::: " +editable.toString()+ ":::"+ isSurePassWord.toString());
            }
        }
    }
    class NameChange implements TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            isSureUsername = Pattern.matches("^[a-zA-Z0-9_]{8,20}",editable.toString());
            if(isSureUsername){
                Log.i(TAG, "isSureUsername ::::" + editable.toString()+"::"+isSureUsername.toString());
            }else {
                Log.i(TAG,"isSureUsername::::   " + editable.toString() +"::  "+ isSureUsername.toString());
            }
        }
    }
}
