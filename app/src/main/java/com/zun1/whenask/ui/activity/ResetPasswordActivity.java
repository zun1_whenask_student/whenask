package com.zun1.whenask.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zun1.whenask.R;
import com.zun1.whenask.ToolsClass.HttpRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText password_now,password_new,password_sure;
    private Button submitBtn,resetBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resetpassword);
        findViewById();
        setOnClickListener();

    }
    void  findViewById(){
        password_new = (EditText)this.findViewById(R.id.resetpassword_new);
        password_now = (EditText)this.findViewById(R.id.resetpassword_now);
        password_sure = (EditText)this.findViewById(R.id.resetpassword_sure);
        submitBtn = (Button)this.findViewById(R.id.resetpassword_submit);
        resetBtn = (Button)this.findViewById(R.id.resetpassword_reset);
    }
    void setOnClickListener(){
        submitBtn.setOnClickListener(this);
        resetBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.e("---------->","xxxx");
        switch (view.getId()){
            case R.id.resetpassword_submit:
                if (password_new.getText().toString().equals(password_sure.getText().toString())){
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            List<Map<String,String>> list = new ArrayList<Map<String, String>>();
                            Map<String,String> usertype = new HashMap<String, String>();
                            usertype.put("usertype","1");
                            list.add(usertype);
                            Map<String,String> userid = new HashMap<String, String>();
                            userid.put("userid","1");
                            list.add(userid);
                            Map<String,String> originpassword = new HashMap<String, String>();
                            originpassword.put("originpassword",password_now.getText().toString().trim());
                            list.add(originpassword);
                            Map<String,String> password = new HashMap<String, String>();
                            password.put("password",password_new.getText().toString().trim());
                            list.add(password);
                            String url = "http://120.76.28.144:7000/mobileapi/public/modifypass/";
                            HttpRequest.postRequestBody(list,url);
                        }
                    }).start();

                }else {
                    Toast.makeText(ResetPasswordActivity.this,"确认密码不一样",Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.resetpassword_reset:
                break;
        }
    }
}
