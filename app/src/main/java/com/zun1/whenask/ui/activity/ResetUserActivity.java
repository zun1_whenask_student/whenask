package com.zun1.whenask.ui.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.zun1.whenask.R;
import com.zun1.whenask.ToolsClass.HttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zun1.whenask.DB.dao.UserDao.Properties.username;

public class ResetUserActivity extends AppCompatActivity {
    private EditText usernameEdit,nameEdit,borthEdit,emailEdit;
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_reset_user);
        findViewById();
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Map<String, String>> list = new ArrayList<Map<String, String>>();
                Map<String, String> useridmap = new HashMap<String, String>();
                useridmap.put("userid", "1");
                list.add(useridmap);
                String url = "http://120.76.28.144:7000/mobileapi/student/profile/";
                result =  HttpRequest.postRequestBody(list,url);
                Log.i("result",result);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject data = new JSONObject(result);
                            String username = data.optString("nickname");
                            String firstname = data.optString("firstname");
                            String birthday = data.optString("birthday");
                            String email = data.optString("email");
                            usernameEdit.setText(username);
                            nameEdit.setText(firstname);
                            borthEdit.setText(birthday);
                            emailEdit.setText(email);
                            Log.i("firstname",firstname);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();

    }
   void findViewById(){
       usernameEdit = (EditText)this.findViewById(R.id.nav_edText_username);
       nameEdit = (EditText)this.findViewById(R.id.nav_edText_name);
       borthEdit = (EditText)this.findViewById(R.id.nav_broth);
       emailEdit = (EditText)this.findViewById(R.id.nav_edText_email);
   }

}
