package com.zun1.whenask.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.zun1.whenask.R;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2016/7/4.
 */
public class SelectTeacher extends AppCompatActivity {

    @BindView(R.id.solveProblem)
    Button solvequestion;
    @BindView(R.id.questionHistory)
    Button questionHistory;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_teacherlayout);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.solveProblem)
    public void solveQuestion(){

    }

    @OnClick(R.id.questionHistory)
    public void history(){

    }
}
