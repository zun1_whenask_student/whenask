package com.zun1.whenask.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.zun1.whenask.R;
import com.zun1.whenask.SetLanguageApplication.SetLanguageApplicationClass;
import com.zun1.whenask.ToolsClass.HttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SetLanguage extends AppCompatActivity implements View.OnClickListener{
    private Button familiarBtn,complexBtn,englishBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_language);
        findViewById();
        setOnClickListener();
    }
    void findViewById(){
       familiarBtn = (Button)this.findViewById(R.id.familiarBtn);
        complexBtn = (Button)this.findViewById(R.id.complexBtn);
        englishBtn = (Button)this.findViewById(R.id.englishBtn);

    }
    void setOnClickListener(){
        familiarBtn.setOnClickListener(this);
        complexBtn.setOnClickListener(this);
        englishBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        SetLanguageApplicationClass setLanguageApplicationClass = (SetLanguageApplicationClass) getApplication();
        Intent intent = new Intent(SetLanguage.this,LoginActivity.class);
        switch (view.getId()){
            case R.id.familiarBtn:

                setLanguageApplicationClass.setLanguage(Locale.SIMPLIFIED_CHINESE);
                startActivity(intent);
                break;
            case R.id.complexBtn:
                setLanguageApplicationClass.setLanguage(Locale.TAIWAN);
                startActivity(intent);
                break;
            case R.id.englishBtn:
                setLanguageApplicationClass.setLanguage(Locale.US);
                startActivity(intent);
                break;
        }
    }
    //下次判断是否登陆
    @Override
    protected void onStart() {
        super.onStart();
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Map<String,String>> list = new ArrayList<Map<String, String>>();
                Map<String,String> mapusertype = new HashMap<String, String>();
                mapusertype.put("usertype","1");
                list.add(mapusertype);
                Map<String,String> mapkeysign = new HashMap<String, String>();
                mapkeysign.put("keysign","1071d8c27fe9b9e3bf931384aaed77ac1ea04fbb");
                list.add(mapkeysign);
                Map<String,String> maplon = new HashMap<String, String>();
                maplon.put("lon","112.1111");
                list.add(maplon);
                Map<String,String> maplat = new HashMap<String, String>();
                maplat.put("lat","32.1111");
                list.add(maplat);
                String url = "http://120.76.28.144:7000/mobileapi/public/autosignin/";
                String result = HttpRequest.postRequestBody(list,url);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String errorCode = jsonObject.optString("ErrorCode");
                    if (errorCode.equals("000")){
                        Intent intent = new Intent(SetLanguage.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
